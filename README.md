# mahi.ma
Mon site web personnel mahi.ma

Bienvenue sur le dépôt du site de Ahmed Mahi.

Ce site est basé sur le template [html5up](https://html5up.net/identity) , hébergé chez  [Netlify](https://www.netlify.com/) géré par moi-même. 

Si vous voyez des fautes, si vous souhaitez ajouter des précisions, n'hésitez pas à cloner le dépot et à me faire des pull requests.

Merci !
